const {Builder, By, Key, until} = require('selenium-webdriver');
const assert = require('assert');


//calculator test + screenshot
test('5*5 = 25', async () => {
    // creating a driver
    const driver = new Builder().forBrowser('chrome').build();
    // get a page
    await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');
    await (await driver.findElement({ name: 'five'})).click();
    await (await driver.findElement({ name: 'mul' })).click();
    await (await driver.findElement({ name: 'five' })).click();
    await (await driver.findElement({ name: 'result' })).click();
 result = await (await driver.findElement({ name: 'Display' })).getAttribute('value');
	assert(result == '25');
	
	driver.takeScreenshot().then(
    function(image, err) {
        require('fs').writeFile('out.png', image, 'base64', function(err) {
            console.log(err);
        });
    }
);
	
    await driver.quit();
});

test('Google should has Google in title', async () => {
    // creating a driver
    const driver = new Builder().forBrowser('chrome').build();
    // get a page
    await driver.get('https://www.google.com/');
    // check if the title contains Google (repeat that process 1 sec. till true)
    await driver.wait(until.titleContains('Google'), 1000);
    await driver.quit();
});

test('Google results should has GitLab link after searching GitLab', async () => {
    // creating a driver
    const driver = new Builder().forBrowser('chrome').build();
    // get a page
    await driver.get('https://www.google.com/');
    // send some keys
    await driver.wait(until.elementLocated(By.name('q')), 1000);
    await (await driver.findElement({ name: 'q' })).sendKeys('gitlab', Key.ENTER);
    // check if link is available
    await driver.wait(until.elementLocated(By.partialLinkText('GitLab.org')), 5000);
    await driver.quit();
});
